/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#include "avwindow.h"
#include "ui_avwindow.h"
#include "GetHash.h"
#include "checking.h"

AVWindow::AVWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AVWindow)
{

    ui->setupUi(this);
}

AVWindow::~AVWindow()
{
    delete ui;
}

void AVWindow::on_AVWCheckFile_clicked()
{
    try
        {
            fileChecker checker;
            QString FileDir = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Files (*.*)"));
            ui->pathFolder->setText(FileDir);
            if(FileDir.isEmpty())
                return;
            checker(FileDir);
            QString viruses = checker.getList().last().toQString();

            if(viruses.isEmpty())
            {
                QPixmap pix(":/4GUI/AVNoneInfect.png");
                ui->NoneInfect->setPixmap(pix);
                QPixmap back(":/4GUI/WinsMDClearV2.png");
                ui->label->setPixmap(back);

                //msgBox.setText("The file is not infected.");
            }
            else
            {
                QPixmap pix(":/4GUI/AVInfect.png");
                ui->NoneInfect->setPixmap(pix);
                QPixmap back(":/4GUI/WinMDInfectV2.png");
                ui->label->setPixmap(back);
                ui->VirusesList->addItem(viruses);
                //msgBox.setText("The file is infected by:\n" + viruses);

            }
        }
        catch(int error)
        {
            return;
        }

}
