/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QResizeEvent>
/*by Punin Viktor*/
#include "GetHash.h"
#include "checking.h"
#include "dirwidget.h"
#include"avwindow.h"
#include "hashwindow.h"
#include "setwin.h"
#include "Lang.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //connect( ui->pushButton, SIGNAL(clicked()), this, SLOT(slotShowHideSlide()));
    // Check Dir Widget
    connect( ui->pushButtonCheckDir, SIGNAL(clicked()), this, SLOT(slotShowHideSlide()));
    m_slideWIdget = new DirWidget(this);
    m_slideWIdget->hide();
    m_geometry = m_slideWIdget->geometry();
    m_boolHide = true;
    //ui->centralWidget->setMouseTracking(true);
    // Check file widget
    connect( ui->checkFileButton, SIGNAL(clicked()), this, SLOT(slotShowHideSlideAV()));
    m_slideWIdgetAV = new AVWindow (this);
    m_slideWIdgetAV->hide();
    m_geometryAV = m_slideWIdgetAV->geometry();
    m_boolHideAV = true;
    //ui->centralWidgetAV->setMouseTracking(true);
    // Check Hash widget
    connect( ui->GetHashButton, SIGNAL(clicked()), this, SLOT(slotShowHideSlideH()));
    m_slideWIdgetH = new HashWindow (this);
    m_slideWIdgetH->hide();
    m_geometryH = m_slideWIdgetH->geometry();
    m_boolHideH = true;
    // Settings slide
    connect( ui->settingsbutton, SIGNAL(clicked()), this, SLOT(slotShowHideSlideSW()));
    m_slideWIdgetSW = new SetWin(this);
    m_slideWIdgetSW->hide();
    m_geometrySW = m_slideWIdgetSW->geometry();
    m_boolHideSW = true;
    // About
    connect( ui->About, SIGNAL(clicked()), this, SLOT(slotShowHideSlideA()));
    m_slideWIdgetA = new AboutUs(this);
    m_slideWIdgetA->hide();
    m_geometryA = m_slideWIdgetA->geometry();
    m_boolHideA = true;


}

MainWindow::~MainWindow()
{
    delete ui;
}

//------------------------------------------------------------------------------
void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (!m_boolHide)
        m_slideWIdget->setGeometry(QRect(0,0, m_slideWIdget->width(), height()));
}
//------------------------------------------------------------------------------
void MainWindow::slotShowHideSlide()
{
    if (m_slideWIdget->isHidden())
        m_slideWIdget->show();

    showHideSlideWidget(m_boolHide);
}
//------------------------------------------------------------------------------
void MainWindow::showHideSlideWidget(bool f_flag)
{
    if (f_flag)
        m_slideWIdget->setGeometry(m_geometry);

    m_animation = new QPropertyAnimation(m_slideWIdget, "geometry");
    m_animation->setDuration(300);

    QRect startRect(210, 80, 0, height());
    QRect endRect(230, 80, m_slideWIdget->width(), height());

    if (f_flag)
    {
        ui->pushButtonCheckDir->setIcon(QIcon(":/4GUI/CheckDirMDEng.png")); // change pic on the button
        m_animation->setStartValue(startRect);
        m_animation->setEndValue(endRect);
    }
    else
    {
        m_animation->setStartValue(endRect);
        m_animation->setEndValue(startRect);
        ui->pushButtonCheckDir->setIcon(QIcon(":/4GUI/CheckEng.png"));
    }
    m_animation->start();
    m_boolHide = !f_flag;

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ///////////////////////////Antivirus slide/////////////////////////////////////////

void MainWindow::resizeEventAV(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (!m_boolHideAV)
        m_slideWIdgetAV->setGeometry(QRect(0,0, m_slideWIdgetAV->width(), height()));
}
//------------------------------------------------------------------------------
void MainWindow::slotShowHideSlideAV()
{
    if (m_slideWIdgetAV->isHidden())
        m_slideWIdgetAV->show();

    showHideSlideWidgetAV(m_boolHideAV);
}
//------------------------------------------------------------------------------
void MainWindow::showHideSlideWidgetAV(bool f_flag)
{
    if (f_flag)
        m_slideWIdgetAV->setGeometry(m_geometryAV);

    m_animationAV = new QPropertyAnimation(m_slideWIdgetAV, "geometry");
    m_animationAV->setDuration(300);

    QRect startRect(210, 80, 0, height());
    QRect endRect(230, 80, m_slideWIdgetAV->width(), height());

    if (f_flag)
    {
        ui->checkFileButton->setIcon(QIcon(":/4GUI/AVMDEng.png"));
        m_animationAV->setStartValue(startRect);
        m_animationAV->setEndValue(endRect);
    }
    else
    {
        m_animationAV->setStartValue(endRect);
        m_animationAV->setEndValue(startRect);
        ui->checkFileButton->setIcon(QIcon(":/4GUI/AntivirusMDEng.png"));
    }
    m_animationAV->start();
    m_boolHideAV = !f_flag;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// ///////////////////////////Hash slide/////////////////////////////////////////
void MainWindow::resizeEventH(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (!m_boolHideH)
        m_slideWIdgetH->setGeometry(QRect(0,0, m_slideWIdgetH->width(), height()));
}
//------------------------------------------------------------------------------
void MainWindow::slotShowHideSlideH()
{
    if (m_slideWIdgetH->isHidden())
        m_slideWIdgetH->show();

    showHideSlideWidgetH(m_boolHideH);
}
//------------------------------------------------------------------------------
void MainWindow::showHideSlideWidgetH(bool f_flag)
{
    if (f_flag)
        m_slideWIdgetH->setGeometry(m_geometryH);

    m_animationH = new QPropertyAnimation(m_slideWIdgetH, "geometry");
    m_animationH->setDuration(300);

    QRect startRect(210, 80, 0, height());
    QRect endRect(230, 80, m_slideWIdgetH->width(), height());

    if (f_flag)
    {
        ui->GetHashButton->setIcon(QIcon(":/4GUI/HashMDEng.png"));
        m_animationH->setStartValue(startRect);
        m_animationH->setEndValue(endRect);
    }
    else
    {
        m_animationH->setStartValue(endRect);
        m_animationH->setEndValue(startRect);
        ui->GetHashButton->setIcon(QIcon(":/4GUI/HASHENGMD.png"));
    }
    m_animationH->start();
    m_boolHideH = !f_flag;
}

// /////////////////////////////////Settings///////////////////////////////////
void MainWindow::resizeEventSW(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (!m_boolHideSW)
        m_slideWIdgetSW->setGeometry(QRect(0,0, m_slideWIdgetSW->width(), height()));
}
//------------------------------------------------------------------------------
void MainWindow::slotShowHideSlideSW()
{
    if (m_slideWIdgetSW->isHidden())
        m_slideWIdgetSW->show();

    showHideSlideWidgetSW(m_boolHideSW);
}
//------------------------------------------------------------------------------
void MainWindow::showHideSlideWidgetSW(bool f_flag)
{
    if (f_flag)
        m_slideWIdgetSW->setGeometry(m_geometrySW);

    m_animationSW = new QPropertyAnimation(m_slideWIdgetSW, "geometry");
    m_animationSW->setDuration(300);

//    QRect startRect(210, 80, 0, height());
//    QRect endRect(230, 80, m_slideWIdgetSW->width(), height());
//    QRect startRect(500, height(), width(), height());
//    QRect endRect(500, height() - m_slideWIdgetSW->height(), width(), m_slideWIdgetSW->height());
    QRect startRect(500, 48, m_slideWIdgetSW->width(), 0);
    QRect endRect(500, 48, m_slideWIdgetSW->width(), m_slideWIdgetSW->height());


    if (f_flag)
    {
        //ui->pushButtonCheckDir->setIcon(QIcon("4GUI/CheckDirMD.png")); // change pic on the button
        m_animationSW->setStartValue(startRect);
        m_animationSW->setEndValue(endRect);
    }
    else
    {
        m_animationSW->setStartValue(endRect);
        m_animationSW->setEndValue(startRect);
        //ui->pushButtonCheckDir->setIcon(QIcon("4GUI/CheckDirOff.png"));
    }
    m_animationSW->start();
    m_boolHideSW = !f_flag;
}
// /////////////////////////////////About slide///////////////////////////////////
void MainWindow::resizeEventA(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    if (!m_boolHideA)
        m_slideWIdgetA->setGeometry(QRect(0,0, m_slideWIdgetA->width(), height()));
}
//------------------------------------------------------------------------------
void MainWindow::slotShowHideSlideA()
{
    if (m_slideWIdgetA->isHidden())
        m_slideWIdgetA->show();

    showHideSlideWidgetA(m_boolHideA);
}
//------------------------------------------------------------------------------
void MainWindow::showHideSlideWidgetA(bool f_flag)
{
    if (f_flag)
        m_slideWIdgetA->setGeometry(m_geometryA);

    m_animationA = new QPropertyAnimation(m_slideWIdgetA, "geometry");
    m_animationA->setDuration(300);

//    QRect startRect(210, 80, 0, height());
//    QRect endRect(230, 80, m_slideWIdgetSW->width(), height());
//    QRect startRect(500, height(), width(), height());
//    QRect endRect(500, height() - m_slideWIdgetSW->height(), width(), m_slideWIdgetSW->height());
    QRect startRect(254, 42, m_slideWIdgetA->width(), 0);
    QRect endRect(254, 42, m_slideWIdgetA->width(), m_slideWIdgetA->height());


    if (f_flag)
    {
        //ui->pushButtonCheckDir->setIcon(QIcon("4GUI/CheckDirMD.png")); // change pic on the button
        m_animationA->setStartValue(startRect);
        m_animationA->setEndValue(endRect);
    }
    else
    {
        m_animationA->setStartValue(endRect);
        m_animationA->setEndValue(startRect);
        //ui->pushButtonCheckDir->setIcon(QIcon("4GUI/CheckDirOff.png"));
    }
    m_animationA->start();
    m_boolHideA = !f_flag;
}
