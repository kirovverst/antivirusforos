#-------------------------------------------------
#
# Project created by QtCreator 2014-11-26T10:46:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = antivirusforos
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    GetHash.cpp \
    fileSystem.cpp \
    checking.cpp \
    ch.cpp \
    dirwidget.cpp \
    avwindow.cpp \
    hashwindow.cpp \
    setwin.cpp \
    aboutus.cpp

HEADERS  += mainwindow.h \
    GetHash.h \
    fileSystem.h \
    functor.h \
    checking.h \
    ch.h \
    dirwidget.h \
    avwindow.h \
    hashwindow.h \
    setwin.h \
    Lang.h \
    aboutus.h

win32:RC_FILE = antivirusforos.rc

FORMS    += mainwindow.ui ch.ui \
    dirwidget.ui \
    avwindow.ui \
    hashwindow.ui \
    setwin.ui \
    aboutus.ui

OTHER_FILES += \
    antivirusforos.rc

RESOURCES += \
    images.qrc
