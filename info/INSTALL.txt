1. INSTALLATION OF SENTINEL:

1.1. Installation from source:

- Check Requirements.
- Uninstall any old version.
- Get the source zip file.
- Unzip the archive to an appropriate location. 

REQUIREMENTS:
Qt tool-kit v5.3 (for developers)
JVM (java virtual machine) - latest version
Windows 7, 8, 8.1

1.2. Installation From Package

Installing your distribution’s package is the easiest route. Open the installer and follow the instructions. It will automatically instal the software on your computer.
ATTENTION! It will not install the source files.

——————————————————————————

2. DIRECTORY STRUCTURE:

The installation process will create several subfolders and files under the main
installation directory:
  
  4GUI/: images used in creating the GUI;
  data/: virus base;
  info/: documentation;
  antivirusforos.exe
  README.md
  LICENSE.txt

——————————————————————————

3. SOURCE FILES DESCRIPTIONS:

GetHash.cpp
GetHash.h
// header and source file for checksum calculating

antivirusforos.pro
antivirusforos.rc
// Qt settings for the project.
// Windows Resource File

ch.ui
ch.cpp
ch.h
// GUI for checking directory dialog window

checking.cpp
checking.h
// header and source file for checking directory

data/virusbase
// example of viruses database

fileSystem.cpp
fileSystem.h
// header and source file for browsing file system

functor.h
// declaration of abstract functional object

list.txt // list of contributors

main.cpp
// Main file of the project containing the main() function.

mainwindow.cpp
mainwindow.h
// Main window’s logics. Describes application response to clicking on certain buttons etc. Used for GUI.

mainwindow.ui // GUI itself.

thefrst.ico // software icon

---------
Copyright © 2014 Antivirus4OS. This file is part of the Sentinel. All rights reserved. Contact us: <av4os.sentinel@gmail.com>
