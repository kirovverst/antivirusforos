/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#ifndef CH_H
#define CH_H


#include <QTimer>
#include "QDialogButtonBox"
#include "checking.h"
#include <QWidget>


namespace Ui {
class Ch;
}

class Ch : public QDialog
{
    Q_OBJECT

public:
    explicit Ch(QWidget *parent = 0);
    ~Ch();

    void addNewRow( ResultOfCheckingFile & );
protected:
    void changeEvent(QEvent *e);
    //void clearTable();
private slots:

    void on_ChosenDirectory_clicked();

    void on_Ok_clicked();

    void on_cancel_clicked();

private:
    Ui::Ch *ui;

    const QString INFECTED = "is infected";
    const QString NOT_INFECTED = "is not infected";
    const int MAX_HEIGHT = 527;
    const int WIDTH = 786;
};

#endif // CH_H
