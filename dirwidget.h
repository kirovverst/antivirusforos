/************************************************************************
**
** Copyright © 2014 Antivirus4OS. All rights reserved.
** Contacts: av4os.sentinel@gmail.com
**
** This file is part of the Sentinel project.
** You may use this file under the terms of the Expat license as follows:
**
** "Permission is hereby granted, free of charge, to any person obtaining
** a copy of this software and associated documentation files (the
** "Software"), to deal in the Software without restriction, including
** without limitation the rights to use, copy, modify, merge, publish,
** distribute, sublicense, and/or sell copies of the Software, and to
** permit persons to whom the Software is furnished to do so, subject to
** the following conditions:
**
** The above copyright notice and this permission notice shall be included
** in all copies or substantial portions of the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
** CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
** TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
** SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."
**
*************************************************************************/
#ifndef DIRWIDGET_H
#define DIRWIDGET_H

#include <QWidget>
#include <QTimer>
#include "QDialogButtonBox"
#include "checking.h"

namespace Ui {
class DirWidget;
}

class DirWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DirWidget(QWidget *parent = 0);
    ~DirWidget();
    void addNewRow( ResultOfCheckingFile & );
private slots:

    void on_ChosenDirectory_clicked();

    void on_Ok_clicked();

    void on_cancel_clicked();

private:
    Ui::DirWidget *ui;
    const QString INFECTED = "is infected";
    const QString NOT_INFECTED = "is not infected";
//        const int MAX_HEIGHT = 527;
//        const int WIDTH = 786;
};

#endif // DIRWIDGET_H
